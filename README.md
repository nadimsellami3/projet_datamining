# Projet DataMining

## Description du Projet

Ce projet vise à exploiter les capacités du langage de programmation Python pour effectuer une opération de Data Mining sur des images provenant de Wikidata. Les principales étapes du projet sont la collecte de données, l'analyse des métadonnées de l'image et la visualisation des données. 

## Partie 1 : Collecte de données

La première étape de ce projet consiste à interroger la base de données Wikidata pour obtenir des images de planètes. Nous utilisons la bibliothèque `requests` de Python, qui est une bibliothèque populaire pour effectuer des requêtes HTTP. 

Nous avons défini un chemin de dossier où nous souhaitons enregistrer les images téléchargées. Une requête HTTP est faite à Wikidata en utilisant une requête SPARQL, un langage de requête pour les bases de données, pour récupérer les identifiants des images ayant des licences ouvertes sur le thème des planètes.

Nous avons également limité le nombre d'images à télécharger pour éviter une surcharge de données.

Ensuite, nous traitons la réponse JSON pour extraire les identifiants d'images. Les images existantes dans le dossier sont supprimées pour assurer une nouvelle collecte de données propre. Enfin, une boucle est utilisée pour télécharger chaque image dans le dossier spécifié.

### Analyse des métadonnées

L'étape suivante consiste à analyser les métadonnées de chaque image téléchargée. Nous utilisons la bibliothèque PIL (Python Imaging Library), une bibliothèque puissante pour ouvrir, manipuler et sauvegarder de nombreux formats de fichiers d'images.

Pour chaque image, nous déterminons son orientation (paysage, portrait ou carré) en comparant la largeur et la hauteur de l'image. Ensuite, nous déterminons la couleur principale de l'image. Pour cela, nous réduisons d'abord la taille de l'image pour accélérer le traitement. Nous convertissons ensuite l'image en une matrice de pixels et calculons la couleur moyenne des pixels. En fonction de la couleur moyenne calculée, nous attribuons une couleur principale à l'image.

Enfin, nous enregistrons les métadonnées, y compris le nom de l'image, le format, la taille, la largeur, la hauteur, l'orientation, la date (si disponible dans les métadonnées EXIF) et la couleur principale, dans un dictionnaire pour chaque image. Nous ajoutons ce dictionnaire à une liste, que nous écrivons ensuite dans un fichier JSON pour une utilisation ultérieure.

### Affichage des données

La dernière étape de ce projet consiste à afficher les métadonnées extraites. Nous utilisons la bibliothèque pandas, une bibliothèque puissante pour la manipulation de données, pour charger le fichier JSON en dataframe. Cela nous permet d'afficher et d'analyser les données de manière plus lisible.
## Partie II: Analyse de Données et Recommandations

Dans cette section, nous avons mis en place le coeur de notre système: l'analyse de données et la recommandation d'images basée sur les goûts de l'utilisateur.

### 2.1 Analyse de Données

Nous avons utilisé la librairie `pickle` pour sauvegarder et charger les données des utilisateurs. Chaque utilisateur est un objet de la classe `Utilisateur`, avec des attributs comme le nom, les images qu'il aime (`likes`) et celles qu'il n'aime pas (`dislikes`). Pour chaque image, on stocke l'identifiant unique de l'image.

Nous avons également ajouté des méthodes à la classe `Utilisateur` pour `ajouter_like` et `ajouter_dislike`, c'est-à-dire pour ajouter une image à la liste des images aimées ou non aimées par l'utilisateur. Ces méthodes mettent automatiquement à jour les données sauvegardées de l'utilisateur.

Enfin, une méthode `recommander` a été ajoutée à la classe `Utilisateur`. Cette méthode analyse les préférences de l'utilisateur en termes de couleur, d'orientation et de taille des images, puis utilise ces informations pour recommander une image que l'utilisateur pourrait aimer.

### 2.2 Visualisation des Données

Pour mieux comprendre les données dont nous disposons, nous avons créé des visualisations de données en utilisant la librairie `matplotlib.pyplot`. Nous avons analysé le nombre d'images disponibles par année, par orientation, par couleur et par taille. Ces visualisations nous aident à comprendre les tendances et les caractéristiques de notre ensemble de données.

Nous avons également mis en place une fonction `diagramme_images_likes` qui prend une liste d'identifiants d'images aimées et affiche des diagrammes similaires pour cet ensemble spécifique d'images.

### 2.3 Système de Recommandation

Avec la méthode `recommander` mise en place, nous sommes maintenant en mesure de recommander des images aux utilisateurs en fonction de leurs goûts. Nous avons testé cette fonctionnalité en recommandant une image à un utilisateur de test. L'image recommandée est affichée à l'aide de `matplotlib.image`.

## Partie III: Objectifs non réussis

Malgré nos progrès, il y a encore des aspects de notre projet que nous n'avons pas encore pu accomplir:

- Nous n'avons pas encore intégré un système de filtrage collaboratif, qui pourrait aider à améliorer la qualité de nos recommandations en utilisant les informations provenant de plusieurs utilisateurs.

- Actuellement, notre système ne tient pas compte du fait que les goûts d'un utilisateur peuvent évoluer avec le temps. Il serait utile d'ajouter un système de pondération temporelle, où les likes et dislikes plus récents ont plus de poids dans les recommandations.

- Il serait également bénéfique de pouvoir recommander plusieurs images à la fois, plutôt qu'une seule.

## Partie IV: Exécution du Code

Voici comment exécuter notre code:

1. Assurez-vous que vous avez toutes les dépendances nécessaires installées, y compris `pickle`, `uuid`, `json`, `matplotlib`, `numpy`, `pandas` et `os`.

2. Lancez le script Python `main.py` à partir de votre terminal en utilisant la commande `python main.py`.

3. Lorsque le script s'exécute, il créera un nouvel utilisateur de test, ajoutera quelques images aux listes des images aimées et non aimées de l'utilisateur, et affichera les listes des images aimées.

4. Le script affiche ensuite les diagrammes de visualisation de données pour l'ensemble de données d'images entier ainsi que pour les images aimées par l'utilisateur.

5. Enfin, le script recommande une image à l'utilisateur et affiche cette image.

Nous espérons que vous apprécierez d'explorer notre projet et d'examiner les recommandations d'images que notre système génère. Bonne exploration des données!

## Installation et Utilisation

### Prérequis

- Python 3.6 ou une version ultérieure
- Bibliothèques Python : os, requests, json, numpy, PIL (Python Imaging Library), pandas

### Installation

Pour installer les bibliothèques nécessaires, vous pouvez utiliser pip, le système de gestion de paquets de Python. Vous pouvez l'installer

 à l'aide de la commande suivante :

```bash
pip install requests numpy pandas pillow
```

### Utilisation

1. Clonez le répertoire du projet à l'aide de la commande suivante :

```bash
git clone <https://gitlab.com/nadimsellami3/projet_datamining.git>
```

2. Exécutez les scripts Python dans l'ordre suivant :

```bash
python collecte_donnees.py
python analyse_metadonnees.py
python affichage_donnees.py

```

## DEMANDE RENDU GLOBAL

### Sources des données
Les images utilisées dans ce projet proviennent de Wikidata. Nous utilisons la bibliothèque requests de Python pour effectuer des requêtes HTTP à Wikidata et récupérer les images.

**Licence des images :** Les images obtenues ont des licences ouvertes sur le thème des planètes.

### Taille des données
La taille exacte des données collectées peut varier en fonction du nombre d'images récupérées. Cependant, nous avons limité le nombre d'images à télécharger pour éviter une surcharge de données. (Vous devrez ajouter le nombre d'images que vous avez collectées ou la taille totale de vos données)

### Informations stockées pour chaque image

Pour chaque image, nous stockons les informations suivantes :
- Le nom de l'image
- Le format de l'image
- La taille de l'image
- La largeur et la hauteur de l'image
- L'orientation de l'image (paysage, portrait ou carré)
- La date de l'image (si disponible dans les métadonnées EXIF)
- La couleur principale de l'image

### Informations concernant les préférences de l'utilisateur

Nous stockons les préférences des utilisateurs en termes d'images qu'ils aiment et qu'ils n'aiment pas. Ces informations sont utilisées pour recommander de nouvelles images à l'utilisateur.

### Modèles d'exploration de données et/ou d'apprentissage machine utilisés

Nous avons utilisé l'analyse de métadonnées pour recommander des images en fonction des préférences de l'utilisateur. Nous n'avons pas utilisé de modèles d'apprentissage machine spécifiques. (Si vous avez utilisé des modèles d'apprentissage machine, veuillez les décrire ici et mentionner les métriques obtenues)

### Auto-évaluation de notre travail

Malgré une prise en main difficile de ce module, qui explique notre retard dans le rendu, nous avons pu prendre compte de l'importance de la manipulation des données tout au long du semestre et nous avons beaucoup appris lors de l'élaboration de ce projet.

### Remarques concernant les séances pratiques, les exercices et les possibilités d'amélioration

Rien a signaler, séances pratiques très illustratives des différentes utilisation des données. 



## Auteurs

Matéo PAIN Nadim SELLAMI Antoine Kleitz

