
from PIL import Image
import numpy
import os
import math
import matplotlib.pyplot as plot
from sklearn import svm
import pandas as pd
import sys
import json
from statistics import mean

def couleurPrincipale(img):
    img = img.resize((int(img.size[0]/2), int(img.size[1]/2)))

    numarray = numpy.array(img.getdata(), numpy.uint8) # Matrice des pixels de l'image

    red=[]  
    green=[]
    blue=[] #création d'une liste pour chaque couleur 

    for i in range (len(numarray)):
        red.append(numarray[i][0]) 
        green.append(numarray[i][1])
        blue.append(numarray[i][2]) #on traite chaque pixel et on l'ajoute à sa couleur

    moy_red=mean(red) 
    moy_green=mean(green) 
    moy_blue=mean(blue) # permet d'obtenir la couleur moyenne, que l'on juge comme étant la couleur principale


    if moy_red>=moy_green and moy_red>=moy_blue: 
        return 'Rouge'
        
    elif moy_green>moy_red and moy_green>=moy_blue:  
        return 'Vert'

    elif moy_blue>moy_red and moy_blue>moy_green:  
       return 'Bleu'

    else:
        return 'Autre'

folder_path = "/home/mateo/Bureau/CPE/Data_mining/Projet/images"

for fichier in os.listdir(folder_path):
     if fichier.endswith(".jpg") or fichier.endswith(".png") or fichier.endswith(".jpeg"):
        img=Image.open(folder_path+"/"+fichier)
        a = couleurPrincipale(img)
        print(a)
